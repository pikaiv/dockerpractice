
import Redis from 'ioredis';

/*
if ( ProcessingInstruction.env.ENV_DOCKERPRACTICE == 'test' ) {
	const redis = new Redis(6379, "ll.yakitamago.info");
} else {
	const redis = new Redis(6379, "host.docker.internal");
}
*/
const redis = new Redis(6379, "ll.yakitamago.info", {password: process.env.REDIS_AUTH_PASSWORD});

//redis.subscribe("ps-destroylink", "ps-createlink", "ps-destroyreso", "ps-destroyfield", "ps-deployreso", "ps-createfield", "ps-portalfracker", "ps-bbdeploy", "ps-bbresult", (err, count) => {
redis.subscribe("ps-destroyfield", "ps-createfield", (err, count) => {
	if (err) {
		// Just like other commands, subscribe() can fail for some reasons,
		// ex network issues.
		console.error("Failed to subscribe: %s", err.message);
	} else {
		// `count` represents the number of channels this client are currently subscribed to.
		console.log(
			`Subscribed successfully! This client is currently subscribed to ${count} channels.`
		);
		var ping_func = function () {
			redis.ping(pong_func);
		};
		var pong_func = function () {
			setTimeout(ping_func, 5000);
		};
		setTimeout(pong_func, 5000);
	}
});

redis.on("message", (channel, message) => {
	var decoded = JSON.parse(message);
	var timestamp = new Date();
	var now_text = timestamp.toLocaleDateString()+' '+((timestamp.getHours()<10)?'0':'')+timestamp.getHours()+':'+((timestamp.getMinutes()<10)?'0':'')+timestamp.getMinutes()+':'+((timestamp.getSeconds()<10)?'0':'')+timestamp.getSeconds();
	timestamp.setTime(decoded.timestamp);
	var datetime_text = timestamp.toLocaleDateString()+' '+((timestamp.getHours()<10)?'0':'')+timestamp.getHours()+':'+((timestamp.getMinutes()<10)?'0':'')+timestamp.getMinutes()+':'+((timestamp.getSeconds()<10)?'0':'')+timestamp.getSeconds();
	var output_text = decoded.agent_name +' '+ decoded.action_text +' '+ decoded.portal_name;

	console.log(`${now_text} ${channel}: ${datetime_text}, ${output_text}`);
});

// There's also an event called 'messageBuffer', which is the same as 'message' except
// it returns buffers instead of strings.
// It's useful when the messages are binary data.
redis.on("messageBuffer", (channel, message) => {
	// Both `channel` and `message` are buffers.
	// console.log(channel, message);
});