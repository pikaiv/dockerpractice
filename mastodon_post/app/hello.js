
import fetch from 'node-fetch';

const params = new URLSearchParams();
params.append('status', 'test text');

const meta = [['Authorization', 'Bearer '+process.env.LLAPP_MASTODON_ACCESS_TOKEN], ['User-Agent', 'yakitama node.js mastodon test']];
// const headers = new Headers(meta);

const response = await fetch('https://mstdn.yakitamago.info/api/v1/statuses', {method: 'POST', headers: meta, body: params});
const data = await response.json();
console.log(data);

